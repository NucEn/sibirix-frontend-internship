import { PasswordLock } from "./passwordLock";
export function init() {
    let id = 1;

    // Создаём локалсторадж если его ещё нет
    if (!localStorage.getItem("passwords")) {
        localStorage.setItem("passwords", "{}");
    }

    const locks = document.querySelector(".locks");

    // Добавляем слушатели событий
    document.querySelector(".button-add").addEventListener("click", () => {
        new PasswordLock(locks, ++id);
    });
}
