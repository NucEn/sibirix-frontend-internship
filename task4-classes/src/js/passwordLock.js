import { Bruteforcer } from "./bruteforcer";

// Класс для любых интеракций с паролем
export class PasswordLock {
    id = 0;
    input = "";
    #password = "123";
    resolved = false;
    constructor(insertPlace, id) {
        this.id = id;

        // Задаём пароль
        let passwords = JSON.parse(localStorage.getItem("passwords"));
        if (passwords[this.id]) {
            this.#password = passwords[this.id];
        } else {
            this.#password = "123";
        }
        this.input = "";

        // Добавляем всё на страницу
        this.newLock = this.drawHtml(insertPlace);
        this.addListeners();
        this.inputDisplay = this.newLock.querySelector(".counter");
    }

    drawHtml(insertPlace) {
        let newLock = insertPlace.appendChild(document.createElement("div"));

        newLock.classList.add("lock");

        const html = `
                    <section class="display-section">
                        <p class="counter"></p>
                    </section>
                    <section class="input-section">
                        <button class="button-number" data-key="1">1</button>
                        <button class="button-number" data-key="2">2</button>
                        <button class="button-number" data-key="3">3</button>
                        <button class="button-number" data-key="4">4</button>
                        <button class="button-number" data-key="5">5</button>
                        <button class="button-number" data-key="6">6</button>
                        <button class="button-number" data-key="7">7</button>
                        <button class="button-number" data-key="8">8</button>
                        <button class="button-number" data-key="9">9</button>
                        <button class="button-number" data-key="0">0</button>
                    </section>
                    <section class="buttons-section">
                        <button class="button-apply">Применить</button>
                        <button class="button-clear">Сбросить</button>
                        <button class="button-bruteforce">Взломать</button>
                    </section>
                    <section class="status">Введите пароль</section>
        `;

        newLock.insertAdjacentHTML("beforeend", html);
        return newLock;
    }

    addListeners() {
        this.newLock
            .querySelector(".input-section")
            .addEventListener("click", (event) => {
                if (isFinite(event.target.dataset.key))
                    this.inputNumber(event.target.dataset.key);
            });

        this.newLock
            .querySelector(".button-clear")
            .addEventListener("click", () => this.clearInput());

        this.newLock
            .querySelector(".button-apply")
            .addEventListener("click", () => this.verifyPassword());

        this.newLock
            .querySelector(".button-bruteforce")
            .addEventListener("click", () => {
                this.bruteforcer = new Bruteforcer(this);
                this.bruteforcer.bruteforce();
            });
    }

    inputNumber(number) {
        // Конкатенируем инпут со введённой цифрой и обновляем
        this.input += number.toString();
        this.inputDisplay.innerHTML = this.input;
    }

    verifyPassword() {
        if (this.#password == this.input) {
            this.newLock.querySelector(".status").innerHTML =
                "Пароль введён верно! Теперь можно сменить пароль";
            // Добавляем кнопку сменить пароль, если её ещё нет
            if (!this.newLock.querySelector(".button-update-password")) {
                let updateButton = this.newLock
                    .querySelector(".buttons-section")
                    .appendChild(document.createElement("button"));
                updateButton.classList.add("button-update-password");
                updateButton.appendChild(
                    document.createTextNode("Сменить пароль")
                );
                updateButton.addEventListener("click", () => {
                    this.changePassword();
                    this.clearInput();
                });
            }
            // Очищаем инпут
            this.input = "";
            this.inputDisplay.innerHTML = "";
            return true;
        } else {
            return false;
        }
    }

    clearInput() {
        // Убираем строчку и кнопку обновления пароля
        this.input = "";
        this.inputDisplay.innerHTML = "";
        if (this.newLock.querySelector(".button-update-password")) {
            this.newLock.querySelector(".button-update-password").remove();
        }
        this.newLock.querySelector(".status").innerHTML = "Введите пароль";
    }

    changePassword() {
        // Меняем пароль на инпут, заносим его в локалсторадж
        if (this.resolved) {
            this.#password = this.input;
            let passwords = JSON.parse(localStorage.getItem("passwords"));
            passwords[this.id] = this.#password;
            localStorage.setItem("passwords", JSON.stringify(passwords));
        }
    }
}
