export class Bruteforcer {
    constructor(lock) {
        this.lock = lock;
    }

    *generateNumber() {
        let num = 0;
        let length = 1;
        while (true) {
            while (num < 10 ** length) {
                const newString = this.addZeroes(num, length);
                yield newString;
                num++;
            }
            num = 1;
            length++;
            console.log("length now is", length);
        }
    }

    waiter(timeout) {
        return new Promise((resolve) => {
            setTimeout(() => resolve(), timeout);
        });
    }

    addZeroes(num, size) {
        let s = num + "";
        while (s.length < size) s = "0" + s;
        return s;
    }

    async bruteforce() {
        const generator = this.generateNumber();

        while (true) {
            const number = generator.next().value;
            console.log("number is ", number);
            // прогон по цифрам
            for (let i = 0; i < number.length; i++) {
                const digit = number.substring(i, i + 1);
                this.lock.inputNumber(digit);
            }

            if (this.lock.verifyPassword()) {
                console.log("Пароль был равен ", number);
                break;
            }

            this.lock.clearInput();
            await this.waiter(0);
        }
    }
}
