(function () {
    // Вывод напечатанного пароля
    const inputDisplay = document.querySelector(".counter");
    // Объект для любых интеракций с паролем
    const passwordInteraction = {
        password: "",
        input: "",
        inputNumber(number) {
            // Конкатинируем инпут со введённой цифрой и обновляем
            this.input += number.toString();
            inputDisplay.innerHTML = this.input;
        },
        verifyPassword() {
            if (this.password == this.input) {
                alert("Ураа");
                // Добавляем кнопку сменить пароль, если её ещё нет
                if (!document.querySelector(".button-update-password")) {
                    let updateButton = document.querySelector(".buttons-section")
                        .appendChild(document.createElement("button"));
                    updateButton.setAttribute("class", "button-update-password")
                    updateButton.appendChild(document.createTextNode("Сменить пароль"));
                    updateButton.addEventListener("click", () => this.changePassword());
                }
                // Очищаем инпут
                passwordInteraction.input = "";
                inputDisplay.innerHTML = "";
            } else {
                alert(":(");
            }
        },
        clearInput() {
            // Убираем строчку и кнопку обновления пароля
            this.input = "";
            document.querySelector(".counter").innerHTML = "";
            if (document.querySelector(".button-update-password")) {
                document.querySelector(".button-update-password").remove();
            }
        },
        changePassword() {
            // Меняем пароль на инпут, заносим его в локалсторадж и очищаем
            this.password = this.input;
            localStorage.setItem("password", this.password)
            this.clearInput();
        },
    }

    const init = () => {
        // Если в локальном хранилище есть пароль, то считываем его
        if (localStorage.getItem("password")) {
            passwordInteraction.password = localStorage.getItem("password");
        } else {
            passwordInteraction.password = "123";
        }
        // Добавляем слушатели событий
        const buttons = document.querySelector(".input-section");
        buttons.onclick = (event) => {
            passwordInteraction.inputNumber(event.target.dataset.key);
        }
        document.addEventListener("keydown", (event) => {
            if (isFinite(event.key)) {
                passwordInteraction.inputNumber(Number(event.key));
            } else if (event.key == "Enter") {
                passwordInteraction.verifyPassword();
            } else if (event.key == "Escape") {
                passwordInteraction.clearInput();
            }
        });
        document.querySelector(".button-clear").addEventListener("click", () => passwordInteraction.clearInput())
        document.querySelector(".button-apply").addEventListener("click", () => passwordInteraction.verifyPassword())
    }

    init();
})();
