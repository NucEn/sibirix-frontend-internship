// Информация об отдельных кораблях
import { defineStore } from 'pinia';
import { ShipType, type Ship } from '@/types/ship';
import { useGameStore } from '@/store/game';
import { GameShipStatus, GameUpdateStatus } from '@/types/gameStatus';
import { useFieldStore } from './fields';
import { useNotificationsStore } from './notifications';
import { RouteLocationNormalizedLoaded } from 'vue-router';
import { axiosQuery } from '@/helpers/axiosQuery';
import router from '@/router';

type ShipStore = {
    myShips: Ship[],
    enemyShips: Ship[],
    clickedShipId: null | ShipType,
}
const SHIPS_TOTAL_HEALTH = 20;

export const useShipStore = defineStore('ship', {
    state: (): ShipStore => {
        return {
            myShips: [
                { id: '4-1', orientation: 'horizontal', damage: 0 },
                { id: '3-2', orientation: 'horizontal', damage: 0 },
                { id: '3-1', orientation: 'horizontal', damage: 0 },
                { id: '2-3', orientation: 'horizontal', damage: 0 },
                { id: '2-2', orientation: 'horizontal', damage: 0 },
                { id: '2-1', orientation: 'horizontal', damage: 0 },
                { id: '1-4', orientation: 'horizontal', damage: 0 },
                { id: '1-3', orientation: 'horizontal', damage: 0 },
                { id: '1-2', orientation: 'horizontal', damage: 0 },
                { id: '1-1', orientation: 'horizontal', damage: 0 },
            ],

            enemyShips: [
                { id: '4-1', damage: 0 },
                { id: '3-2', damage: 0 },
                { id: '3-1', damage: 0 },
                { id: '2-3', damage: 0 },
                { id: '2-2', damage: 0 },
                { id: '2-1', damage: 0 },
                { id: '1-4', damage: 0 },
                { id: '1-3', damage: 0 },
                { id: '1-2', damage: 0 },
                { id: '1-1', damage: 0 },
            ],

            clickedShipId: null,
        };
    },

    actions: {
        initializeShips() {
            this.myShips = [
                { id: '4-1', orientation: 'horizontal', damage: 0 },
                { id: '3-2', orientation: 'horizontal', damage: 0 },
                { id: '3-1', orientation: 'horizontal', damage: 0 },
                { id: '2-3', orientation: 'horizontal', damage: 0 },
                { id: '2-2', orientation: 'horizontal', damage: 0 },
                { id: '2-1', orientation: 'horizontal', damage: 0 },
                { id: '1-4', orientation: 'horizontal', damage: 0 },
                { id: '1-3', orientation: 'horizontal', damage: 0 },
                { id: '1-2', orientation: 'horizontal', damage: 0 },
                { id: '1-1', orientation: 'horizontal', damage: 0 },
            ];
        },

        async placeShip(ship: Ship) {
            const params = new URLSearchParams();

            const keys = ['x', 'y', 'ship'];

            for (let key of keys) {
                const typedKey = key as keyof Ship; // указание тайпскрипту

                if (ship[typedKey] !== undefined) {
                    params.append(key, ship[typedKey]?.toString() ?? '');
                }
            }

            if (ship['id']) {
                params.append('ship', ship['id']?.toString() ?? '');
            }
            console.log(ship);


            if (ship['orientation']) {
                params.append('orientation', ship['orientation'].toString() ?? '');
                console.log(1);

            } else {
                params.append('orientation', 'horizontal');
            }

            const response = await axiosQuery<GameUpdateStatus>('place-ship', router.resolve({name: 'battle', params: {gameId: this.game.gameId, playerCode: this.game.playerCode}}), params);

            if (!response || response == null) {return;}

            const shipLocal = this.myShips.find((element: Ship) => {
                return element.id == ship.id;
            });

            if (shipLocal) {
                Object.assign(shipLocal, {
                    x: ship.x,
                    y: ship.y,
                    orientation: ship.orientation,
                });
            }

            return response;
        },

        async placeRandomShips(thisRouter: RouteLocationNormalizedLoaded) {
            let fieldLocal = Array(10).fill(0).map(() => Array(10).fill(0));

            await this.field.clearField(thisRouter);

            for (let ship of this.myShips) {
                const length = Number(ship['id'][0]);

                // Массив, где нули это валидное пространство для корабля, а единицы - нет
                // Единицы заполняются вокруг корабля
                if (ship.id[0] == '1') ship.orientation = 'horizontal';
                else ship.orientation = Math.random() > 0.5 ? 'vertical' : 'horizontal';
                let iteration = 0;

                while (true) {
                    let x;
                    let y;

                    if (ship.orientation == 'horizontal') {
                        x = Math.floor(Math.random() * (11 - length));
                        y = Math.floor(Math.random() * (10));
                    } else {
                        x = Math.floor(Math.random() * (10));
                        y = Math.floor(Math.random() * (11 - length));
                    }

                    if (iteration++ > 1000) break;

                    // Проверка, можно ли вставить корабль
                    if (!this.canInsertShip(ship, fieldLocal, x, y, length)) { continue; }

                    ship.x = x as Ship['x'];
                    ship.y = y as Ship['y'];

                    // Заполнение единицами, если место валидное
                    this.fillInvalidCells(ship, x, y, fieldLocal, length);
                    break;
                }

                // сделать через while
                // оптимизировать, рандомя только из оставшихся координат
                if (iteration > 1000) {
                    this.placeRandomShips(thisRouter);

                    return;
                }
            }

            // Формируем запрос
            const formData = new FormData;

            for (let i = 0; i < this.myShips.length; i++) {
                const ship = this.myShips[i];

                for (const field in ship) {
                    if (!ship.hasOwnProperty(field)) continue;
                    const key = (field == 'id') ? 'ship' : field;

                    formData.append(`ships[${i}][${key}]`, ship[field as keyof Ship] as string);
                }
            }

            const response = await axiosQuery<GameShipStatus>('place-ship', thisRouter, formData);

            if (!response || response == null) {return;}

            return response;
        },

        canInsertShip(ship: Ship, fieldLocal: number[][], x: number, y: number, length: number) {
            for (let cell = 0; cell < length; cell++) {
                if (ship.orientation == 'horizontal') {
                    if (fieldLocal[x + cell][y] == undefined || fieldLocal[x + cell][y] == 1) {
                        return false;
                    }
                }
                else {
                    if (fieldLocal[x][y + cell] == undefined || fieldLocal[x][y + cell] == 1) {
                        return false;
                    }
                }
            }

            return true;
        },

        fillInvalidCells(ship: Ship, x: number, y: number, fieldLocal: number[][], length: number) {
            const width = ship.orientation == 'horizontal' ? length : 1;
            const height = ship.orientation == 'vertical' ? length : 1;

            for (let posX = x - 1; posX <= x + width; posX++) {
                for (let posY = y - 1; posY <= y + height; posY++) {
                    if (posX >= 0 && posY >= 0 && posX <= 9 && posY <= 9) {
                        fieldLocal[posX][posY] = 1;
                    }
                }
            }
        },

        async rotateShip(ship: Ship) {
            if (ship['id'][0] == '1') return;

            const params = new URLSearchParams();
            const keys = ['x', 'y'];

            for (let key of keys) {
                const typedKey = key as keyof Ship;

                if (ship[typedKey] !== undefined) {
                    params.append(key, ship[typedKey]?.toString() ?? '');
                }
            }

            const newOrientation = (ship.orientation == 'horizontal') ? 'vertical' : 'horizontal';

            params.append('orientation', newOrientation);

            params.append('ship', ship['id']?.toString() ?? '');


            const response = await axiosQuery<GameUpdateStatus>('place-ship', router.resolve({name: 'battle', params: {gameId: this.game.gameId, playerCode: this.game.playerCode}}), params);

            if (!response || response == null) {return;}



            const shipLocal = this.myShips.find((element: Ship) => {
                return element.id == ship.id;
            });

            if (shipLocal) {
                shipLocal.orientation = newOrientation;
            }

            this.game.getGameStatus(router.resolve({name: 'battle', params: {gameId: this.game.gameId, playerCode: this.game.playerCode}}));

            return response;
        },

        getShipsFromField(fieldMy: GameUpdateStatus['fieldMy'], fieldEnemy: GameUpdateStatus['fieldEnemy'], usedPlaces: GameUpdateStatus['usedPlaces']) {
            // Ищем, пока не найдём все корабли
            let remainingShips = usedPlaces !== undefined ? usedPlaces : this.myShips.map((ship) =>  ship.id);

            if (remainingShips == null) return;

            for (let row = 0; row < fieldMy.length; row++) {
                fieldMy[row]
                    .find((cell, col) => {
                        if (remainingShips.includes(cell[0])) {
                            const shipLocal = this.myShips.find((el) => {
                                return el.id == cell[0];
                            });

                            if (shipLocal !== undefined) {
                                // Проверяем на вертикальность
                                if (shipLocal.id[0] == '1') {
                                    shipLocal.orientation = 'horizontal';
                                } else if (col > 9 || row < 9 && fieldMy[row + 1][col][0] == fieldMy[row][col][0]) {
                                    shipLocal.orientation = 'horizontal';
                                } else {
                                    shipLocal.orientation = 'vertical';
                                }
                                shipLocal.x = row as Ship['x'];
                                shipLocal.y = col as Ship['y'];
                            }

                            // Убираем из временного массива найденный корабль
                            remainingShips.splice(remainingShips.indexOf(cell[0]), 1);
                        }
                    });
            }

            if (remainingShips.length == 0) return;

            const notificationsStore = useNotificationsStore();

            notificationsStore.createNotification({ type: 'error', text: 'Что-то пошло не так. Попробуйте перезагрузить страницу или начать игру заново', animationTime: 3000 });
        },

        getShipByCoordinates(x: number, y: number) {
            return this.myShips.find((ship) => {
                return ship.x == x && ship.y == y;
            });
        },
    },

    getters: {
        game() {
            const gameStore = useGameStore() as any;

            return gameStore.gameInfo;
        },

        field() {
            const fieldStore = useFieldStore() as any;

            return fieldStore.fieldInfo;
        },

        shipsInfo: (state) => {
            return state;
        },

        myShipsDamagePercentage: (state) => {
            return state.myShips.reduce((value, ship) => value + ship.damage, 0) / SHIPS_TOTAL_HEALTH * 100;
        },

        enemyShipsDamagePercentage: (state) => {
            return state.enemyShips.reduce((value, ship) => value + ship.damage, 0) / SHIPS_TOTAL_HEALTH * 100;
        },

        myShipsReady: (state) => {
            return state.myShips.find((ship) => ship.x == undefined) == undefined;
        },
    },
});
