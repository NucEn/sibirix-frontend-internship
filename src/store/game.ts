import { defineStore } from 'pinia';
import { RouteLocationNormalizedLoaded } from 'vue-router';

import router from '@/router/index';
import { GameStartStatus, GameUpdateStatus } from '@/types/gameStatus';
import { CellStatus, Ship } from '@/types/ship';
import { CellVisibility } from '@/types/fields';
import { axiosQuery } from '@/helpers/axiosQuery';
import { GameStatus } from '@/enums/GameStatus';

import { useFieldStore } from './fields';
import { useShipStore } from './ships';
import { useNotificationsStore } from './notifications';

export type GameState = {
    gameId: string,
    playerCode: string,
    status: number,
    inviteLink: string,
    meReady: boolean,
    myTurn: boolean,
}

export const useGameStore = defineStore('game', {
    state: (): GameState => {
        return {
            gameId: '',
            playerCode: '',
            status: 1,
            inviteLink: '',
            meReady: false,
            myTurn: false,
        };
    },

    actions: {
        resetGame() {
            this.gameId = '';
            this.playerCode = '';
            this.status = GameStatus.Preparation;
            this.inviteLink = '';
            this.meReady = false;
            this.myTurn = false;
        },

        async startGame() {
            this.ships.initializeShips();

            const response = await axiosQuery<GameStartStatus>('start');

            if (!response) {return;}

            this.gameId = response.id;
            this.inviteLink = response.invite;
            this.playerCode = response.code;

            router.push({
                name: 'preparation',
                params: {
                    gameId: this.getCurrentId(),
                    playerCode: this.getCurrentPlayer(),
                },
            });

            return response;
        },

        createStatusWatcher(thisRouter: RouteLocationNormalizedLoaded) {
            let started;

            if (!started) {
                setInterval(() => {
                    if (thisRouter.params.name !== 'home') {this.getGameStatus(thisRouter);}
                }, 5000);
            }

        },

        async getGameStatus(thisRouter: RouteLocationNormalizedLoaded) {
            if (router.currentRoute.value.name == 'home') return;

            const response = await axiosQuery<GameUpdateStatus>('status', thisRouter);

            if (!response) {return;}

            this.gameId = response.game.id;
            this.inviteLink = response.game.invite;
            this.playerCode = thisRouter.params.playerCode as string;
            this.status = response.game.status;
            this.meReady = response.game.meReady;

            if (response.game.myTurn == true && response.game.myTurn !== this.myTurn && response.game.status == GameStatus.Battle) {
                const notificationsStore = useNotificationsStore();

                notificationsStore.createNotification({ type: 'normal', text: '-Твой ход, тысяча чертей!', animationTime: 3000 });
            }
            this.myTurn = response.game.myTurn;

            this.field.fieldMy = response.fieldMy;
            this.field.fieldEnemy = response.fieldEnemy;
            this.field.usedPlaces = response.usedPlaces;

            this.updateDamage();

            this.ships.getShipsFromField(response.fieldMy, response.fieldEnemy, response.usedPlaces);

            this.checkRoute(thisRouter);

            return response;
        },

        checkRoute(thisRouter: RouteLocationNormalizedLoaded) {
            if (this.status == GameStatus.Preparation && thisRouter.name !== 'preparation') {
                router.push(
                    {
                        name: 'preparation',
                        params: {
                            gameId: this.getCurrentId(),
                            playerCode: this.getCurrentPlayer(),
                        },
                    },
                );
            }

            if (this.status == GameStatus.Battle && thisRouter.name !== 'battle') {
                router.push(
                    {
                        name: 'battle',
                        params: {
                            gameId: this.getCurrentId(),
                            playerCode: this.getCurrentPlayer(),
                        },
                    },
                );
            }

            if (this.status == GameStatus.Results && thisRouter.name !== 'results') {
                router.push(
                    {
                        name: 'results',
                        params: {
                            gameId: this.getCurrentId(),
                            playerCode: this.getCurrentPlayer(),
                        },
                    },
                );
            }
        },

        updateDamage() {
            // Обновление урона у кораблей врага
            this.ships.enemyShips.forEach((ship: Ship) => { ship.damage = 0;});
            this.field.fieldEnemy.flat().forEach((cell: [CellStatus, CellVisibility]) => {
                if (cell[0] != 'empty' && cell[0] != 'hidden') {
                    this.ships.enemyShips.forEach((ship: Ship) => {
                        if (ship['id'] == cell[0]) ship.damage++;
                    });
                }
            });

            // Обновление урона у своих кораблей
            this.ships.myShips.forEach((ship: Ship) => { ship.damage = 0;});
            this.field.fieldMy.flat().forEach((cell: [CellStatus, CellVisibility]) => {
                if (cell[0] != 'empty' && cell[0] != 'hidden') {
                    this.ships.myShips.forEach((ship: Ship) => {
                        if (ship['id'] == cell[0] && cell[1] == 1) ship.damage++;
                    });
                }
            });
        },

        getCurrentId(): string {
            if (!this.gameId || !this.playerCode) {
                this.startGame();
            }

            return this.gameId;
        },

        getCurrentPlayer(): string {
            if (!this.gameId || !this.playerCode) {
                this.startGame();
            }

            return this.playerCode;
        },

        async ready(thisRouter: RouteLocationNormalizedLoaded) {
            const response = await axiosQuery<GameUpdateStatus>('ready', thisRouter);

            if (!response) {return;}


            if (response.enemyReady == true) {
                router.push({name: 'battle',
                    params: {
                        gameId: this.getCurrentId(),
                        playerCode: this.getCurrentPlayer(),
                    }},
                );
            }

            else {
                this.meReady = true;
            }


        },

        async shoot(x: Ship['x'], y: Ship['y'], thisRouter: RouteLocationNormalizedLoaded) {
            const params = new URLSearchParams();

            if (x !== undefined && y !== undefined) {
                params.append('x', x.toString());
                params.append('y', y.toString());
            }

            const response = await axiosQuery<GameUpdateStatus>('shot', thisRouter, params);

            if (!response) {return;}

            this.getGameStatus(thisRouter);

        },
    },

    getters: {
        // Подумать, что с этим сделать
        field() {
            const fieldStore = useFieldStore() as any;

            return fieldStore.fieldInfo;
        },

        gameInfo: (state) => {
            return state;
        },

        ships() {
            const shipStore = useShipStore() as any;

            return shipStore.shipsInfo;
        },
    },
});
