import { defineStore } from 'pinia';
import { nextTick } from 'vue';

type Notification = {
    type: 'normal' |'error',
    text: string,
    animationTime: number,
}
type NotificationStore = {
    currentNotification: null | Notification;
    currentTimeout: null | ReturnType<typeof setTimeout>;
}

export const useNotificationsStore = defineStore('notifications', {
    state: (): NotificationStore => {
        return {
            currentNotification: null,
            currentTimeout: null,
        };
    },

    actions: {
        async createNotification(notification: Notification) {
            clearTimeout(this.currentTimeout as ReturnType<typeof setTimeout>);
            this.currentNotification = null;
            await nextTick();
            this.currentNotification = notification;
            this.currentTimeout = setTimeout(() => this.currentNotification = null, notification.animationTime);
        },
    },

    getters: {
        notificationInfo: (state) => {
            return state;
        },
    },
});
