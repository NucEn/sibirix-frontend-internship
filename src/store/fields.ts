import { defineStore } from 'pinia';
import { RouteLocationNormalizedLoaded } from 'vue-router';

import { axiosQuery } from '@/helpers/axiosQuery';
import { FieldStore } from '@/types/fields';
import { GenericResponseStatus } from '@/types/gameStatus';

import { useGameStore } from './game';
import { useShipStore } from './ships';

export const useFieldStore = defineStore('field', {
    state: (): FieldStore => {
        return {
            fieldMy: [],
            fieldEnemy: [],
            usedPlaces: [],
        };
    },

    actions: {
        initializeFields() {
            this.fieldMy = [];
            this.fieldEnemy = [];
            this.usedPlaces = [];
        },

        async clearField(thisRouter: RouteLocationNormalizedLoaded) {
            const response = await axiosQuery<GenericResponseStatus>('clear-field', thisRouter);

            if (!response || response == null) {return;}

            const shipsStore = useShipStore();

            shipsStore.initializeShips();

            return response;
        },
    },

    getters: {
        game() {
            const gameStore = useGameStore();

            return gameStore.gameInfo;
        },
        ships() {
            const shipStore = useShipStore();

            return shipStore.shipsInfo;
        },
        fieldInfo: (state) => {
            return state;
        },
    },
});

// Информация о своих клетках и клетках противника
