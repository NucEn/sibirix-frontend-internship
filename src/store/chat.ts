import { defineStore } from 'pinia';
import { RouteLocationNormalizedLoaded } from 'vue-router';

import router from '@/router';
import { axiosQuery } from '@/helpers/axiosQuery';

type Message = {
    my: boolean,
    time: string,
    message: string
}

type MessageResponse = {
    messages: Message[],
    lastTime: number,
    success: boolean,
    message: string,
}

type ChatStore = {
    messages: Message[],
    lastTime: string,

    typedMessage: string,
}

export const useChatStore = defineStore('chat', {
    state: (): ChatStore => {
        return {
            messages: [],
            lastTime: '',

            typedMessage: '',
        };
    },

    actions: {
        createMessagesWatcher(thisRouter: RouteLocationNormalizedLoaded) {
            let created;

            if (!created) {
                created = true;
                setInterval(() => {if (router.currentRoute.value.name !== 'home') this.getMessages(thisRouter);
                }, 5000);
            }

        },

        async getMessages(thisRouter: RouteLocationNormalizedLoaded) {
            const params = new URLSearchParams();

            if (this.lastTime) {
                params.append('lastTime', this.lastTime.toString());
            }
            const response = await axiosQuery<MessageResponse>('chat-load', thisRouter, params, true);

            if (!response || response == null) {return;}

            this.lastTime = response.lastTime.toString();
            this.messages.push(...response.messages);

        },

        async sendMessage(thisRouter: RouteLocationNormalizedLoaded) {
            if (!this.typedMessage) return;

            const params = new URLSearchParams();

            params.append('message', this.typedMessage);
            this.typedMessage = '';

            const response = await axiosQuery<MessageResponse>('chat-send', thisRouter, params);

            if (!response || response == null) {return;}

            this.getMessages(thisRouter);
        },
    },
});
