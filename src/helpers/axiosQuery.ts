import { useNotificationsStore } from '@/store/notifications';
import axios from 'axios';
import { RouteLocationNormalizedLoaded } from 'vue-router';

const API_BASE_URL = import.meta.env.VITE_API_BASE_URL;

export async function axiosQuery<T>(url: string, route?: RouteLocationNormalizedLoaded, params?: any, get?: boolean) {
    let res;

    if (url == 'start') {
        res = await axios({
            method: 'post',
            auth: {
                username: 'yandex',
                password: 'uhodi',
            },
            headers: {
                'Access-Control-Allow-Origin': '*',
            },
            url: `${API_BASE_URL}/${url}/`,
        });

    } else if (get) {
        if (!route) return null;
        res = await axios({
            method: 'get',
            auth: {
                username: 'yandex',
                password: 'uhodi',
            },
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
            },
            url: `${API_BASE_URL}/${url}/${route.params.gameId}/${route.params.playerCode}`,
            params: params,
        });

    } else {
        if (!route) return null;
        res = await axios({
            method: 'post',
            auth: {
                username: 'yandex',
                password: 'uhodi',
            },
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
            },
            url: `${API_BASE_URL}/${url}/${route.params.gameId}/${route.params.playerCode}`,
            data: params,
        });
    }

    if (!res.data.success) {
        const notificationsStore = useNotificationsStore();

        if (res.data.message) {
            notificationsStore.createNotification({ type: 'error', text: res.data.message, animationTime: 3000 });
        }

        return null;
    }

    return res.data;
}
