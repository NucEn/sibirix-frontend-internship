export const getRandomInt = (min: number, max: number) => {
    min = Math.ceil(min);
    max = Math.floor(max);

    return Math.floor(Math.random() * (max - min + 1)) + min;
};

export const randomProperty = (obj: object) => {
    let result;
    let count = 0;

    for (let prop in obj) {
        // @ts-ignore
        if (Math.random() < 1/++count) result = obj[prop];
    }

    if (!result) return 'black';


    return result;
};
