export enum GameStatus {
    Preparation = 1,
    Battle = 2,
    Results = 3,
}
