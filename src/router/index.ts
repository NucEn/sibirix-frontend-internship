import { createRouter, createWebHistory } from 'vue-router';
import HomeView from '@/views/HomeView.vue';
import PreparationView from '@/views/PreparationView.vue';
import NotFoundView from '@/views/NotFoundView/NotFoundView.vue';
import BattleView from '@/views/BattleView.vue';
import ResultsView from '@/views/ResultsView.vue';

const router = createRouter({
    history: createWebHistory(import.meta.env.BASE_URL),
    routes: [
        {
            path: '/',
            name: 'home',
            component: HomeView,
        },
        {
            path: '/preparation/:gameId/:playerCode',
            name: 'preparation',
            component: PreparationView,
        },
        {
            path: '/game/:gameId/:playerCode',
            name: 'battle',
            component: BattleView,
        },
        {
            path: '/results/:gameId/:playerCode',
            name: 'results',
            component: ResultsView,
        },
        {
            path: '/404',
            name: 'NotFound',
            component: NotFoundView,
        },
        {
            path: '/:pathMatch(.*)*',
            redirect: '/404',
        },
    ],
});

export default router;
