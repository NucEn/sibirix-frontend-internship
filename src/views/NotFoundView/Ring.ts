import { randomProperty, getRandomInt } from '@/helpers/random';
import { Stick } from './Stick';

const RADIUS = 30;
const BORDER_WIDTH = 10;

const BLOW_FORCE = 2e7;
const ELASTICITY = .3;

export class Ring {
    x = 100;
    vx = 0;
    y = 100;
    vy = 0;
    radius = RADIUS;
    borderWidth = BORDER_WIDTH;
    color = 'blue';
    boundary = {
        x: [] as number[],
        y: [] as number[],
    };
    inStick: 0 | Stick = 0;

    blowForce = BLOW_FORCE;
    // Почему гравитация ноль - смотреть в game.ts
    gravity = 0;
    elasticity = ELASTICITY;

    constructor(ctx: CanvasRenderingContext2D, colors: object) {
        this.color = randomProperty(colors);
        this.x = getRandomInt(this.radius, ctx.canvas.width - this.radius);
        this.y = getRandomInt(this.radius, ctx.canvas.height - this.radius);

        this.vx = getRandomInt(-5, 5);
        this.vy = getRandomInt(-5, 5);

        this.drawRing(ctx);
    }

    drawRing(ctx: CanvasRenderingContext2D) {
        ctx.beginPath();

        ctx.arc(this.x, this.y, this.radius, 0, Math.PI * 2, true);


        ctx.arc(this.x, this.y, this.radius - this.borderWidth, 0, Math.PI * 2, true);

        ctx.fillStyle = this.color;
        ctx.fill('evenodd');
    }

    drawUpperRing(ctx: CanvasRenderingContext2D) {
        ctx.beginPath();

        ctx.arc(this.x, this.y, this.radius, 0, Math.PI, true);


        ctx.arc(this.x, this.y, this.radius - this.borderWidth, 0, Math.PI, true);

        ctx.fillStyle = this.color;
        ctx.fill('evenodd');
    }

    drawLowerRing(ctx: CanvasRenderingContext2D) {
        ctx.beginPath();
        ctx.arc(this.x, this.y, this.radius, Math.PI, Math.PI * 2, true);
        ctx.arc(this.x, this.y, this.radius - this.borderWidth, Math.PI, Math.PI * 2, true);

        ctx.fillStyle = this.color;
        ctx.fill('evenodd');
    }

    updatePosition() {
        this.checkWalls();
        // Перемещение на заданное ускорение
        this.x += this.vx;
        this.y += this.vy;

        // Замедление по x
        this.vx *= .99;

        // Замедление по y, если летит вверх,
        // в противном случае будет наоборот ускоряться вниз
        if (this.vy < 0) this.vy *= .99;

        // Костыль для работы гравитации с акселерометром, чтобы
        // при неподвижном наклоне телефона в точке пика кольцо начало падать
        // всё равно работает плохо на телефоне, нужно будет
        // фиксить, если будет использоваться датчик
        if (this.vy >= -0.01 && this.vy < 0.01) this.vy += .015;

        // Обычное падение
        this.vy += this.gravity;
    }

    checkWalls() {
        // Проверка коллизий с boundary
        if (this.y + this.radius + this.vy > this.boundary.y[1] || this.y - this.radius + this.vy < this.boundary.y[0]) {
            this.vy = -this.vy * this.elasticity;
        }

        if (this.x + this.radius + this.vx > this.boundary.x[1] || this.x - this.radius + this.vx < this.boundary.x[0]) {
            this.vx = -this.vx * this.elasticity;
        }

        // Сдвигание к центру палки, если кольцо на палке
        if (this.inStick && this.x >  this.boundary.x[0] + (this.boundary.x[1] - this.boundary.x[0]) / 2) {
            this.vx -= .1;
        }

        if (this.inStick && this.x <  this.boundary.x[0] + (this.boundary.x[1] - this.boundary.x[0]) / 2) {
            this.vx += .1;
        }
    }

    getForce(x: number, y: number) {
        // Получить силу дуновения
        return this.blowForce / (Math.pow((((x - this.x) ** 2 + (y - this.y) ** 2)), 2));
    }

    blow(x: number, y: number) {
        // Дунуть на текущее кольцо
        const force = this.getForce(x, y);

        this.vx += force * (this.x - x);

        this.vy += force * (this.y - y);
    }
}
