
import { Ring } from './Ring';
import { Stick } from './Stick';


export class Game {
    rings: Ring[] = [];
    sticks: Stick[] = [];
    canvas: HTMLCanvasElement;
    ctx: CanvasRenderingContext2D;
    colors: object = {};

    acceleration = {
        x: 0,
        y: 0,
    };

    tilt = {
        x: 0,
        y: 0,
    };

    constructor(_canvas: HTMLCanvasElement, colors: object) {
        this.canvas = _canvas;
        this.ctx = this.canvas.getContext('2d') as CanvasRenderingContext2D;

        this.colors = colors;
        this.createRings();
        this.createSticks();
        this.addEventListeners();

        // !!! Здесь комментарии, потому что работа с сенсорами ещё кривая, по желанию добработать их
        // и раскомментировать блок ниже

        // if (this.isPhone()) {
        //     this.addSensors();
        // }
        // else {
        this.rings.forEach((ring) => {
            // Костыль для гравитации - с телефона управляется лишь наклоном, с ПК это константа
            // по желанию сделать грамотнее
            ring.gravity = .015;
        });
        // }

        this.updateRender();
    }

    createRings(count: number = 14) {
        for (let index = 0; index < count; index++) {
            this.rings.push(new Ring(this.ctx, this.colors));
        }
    }

    createSticks() {
        // Здесь будет необходимо понять нормальные координаты палок
        this.sticks.push(new Stick(this.canvas.width * 1/5 + 3, this.canvas.height * 1/3));
        this.sticks.push(new Stick(this.canvas.width * 3/5 - 18, this.canvas.height * 1/3));
    }

    updateRender() {
        this.defineBoundaries();
        this.ctx.clearRect(0,0, this.canvas.width, this.canvas.height);

        const ringsOnSticksByOrder = this.rings.filter((ring) => ring.inStick !== 0).sort((a, b) => b.y - a.y);
        const ringsFree = this.rings.filter((ring) => ring.inStick == 0);

        // Рендерим в таком порядке для того, чтобы всё было по слоям
        // Есть проблема с "отставанием" на один пиксель при падении
        // но её судя по всему нельзя исправить
        ringsOnSticksByOrder.forEach((ring) => ring.drawUpperRing(this.ctx));
        this.sticks.forEach((stick) => stick.drawStick(this.ctx));
        ringsOnSticksByOrder.forEach((ring) => ring.drawLowerRing(this.ctx));

        ringsFree.forEach((ring) => ring.drawRing(this.ctx));

        this.rings.forEach((ring) => ring.updatePosition());

        window.requestAnimationFrame(this.updateRender.bind(this));
    };

    addEventListeners() {
        // Слушатель на победу, по желанию поменять логику
        const winInterval = setInterval(() => {
            if (this.rings.filter((ring) => ring.inStick).length == this.rings.length) {
                setTimeout(() => {
                    if (this.rings.filter((ring) => ring.inStick).length == this.rings.length) {
                        // Здесь вызвать нужный метод, который вызывает модальку
                        alert('Победа!');
                        clearInterval(winInterval);
                    }
                }, 1000);
            }
        }, 2000);

        // Слушатель на нажатие шифтов
        document.addEventListener('keyup', (e: KeyboardEvent) => {
            if (e.code == 'ShiftLeft') {
                this.blowAll(50, this.canvas.height + 40);
            }

            if (e.code == 'ShiftRight') {
                this.blowAll(this.canvas.width - 50, this.canvas.height + 40);
            }

        });
    }

    isPhone() {
        let hasTouchScreen = false;

        if ('maxTouchPoints' in navigator) {
            hasTouchScreen = navigator.maxTouchPoints > 0;

            if (hasTouchScreen) {
                return true;
            }

            return false;
        }
    }

    addSensors() {
        let accelerometer: Accelerometer | null = null;

        // Добавляем акселерометр, если можем. Проверка скопипащенная с MDN,
        // возможно устаревшая или можно попроще
        try {
            accelerometer = new Accelerometer({ referenceFrame: 'device' });
            accelerometer.addEventListener('error', (event) => {
                // Handle runtime errors.
                if (event.error.name === 'NotAllowedError') {
                    // Branch to code for requesting permission.
                } else if (event.error.name === 'NotReadableError' ) {
                    console.log('Cannot connect to the sensor.');
                }
            });
            accelerometer.addEventListener('reading', () => this.changeAcceleration(accelerometer as Accelerometer));
            accelerometer.start();
        } catch (error: unknown) {
            // Handle construction errors.
            if (error instanceof Error && error.name === 'SecurityError') {
                // See the note above about feature policy.
                console.log('Sensor construction was blocked by a feature policy.');
            } else if (error instanceof Error && error.name === 'ReferenceError') {
                console.log('Sensor is not supported by the User Agent.');
            } else {
                throw error;
            }
        }

        //  Если можем добавить акселерометр, то добавляем и гироскоп
        if (accelerometer !== null) {
            const gyroscope = new Gyroscope({referenceFrame: 'device', frequency: 60});

            gyroscope.addEventListener('reading', () => {
                this.changeTilt(gyroscope);
            });

            gyroscope.start();
        }
    }

    changeAcceleration(accelerometer: Accelerometer) {
        // Протестить как следует, разобраться, почему TS выдаёт ошибку
        if (accelerometer.x !== undefined) {
            this.rings.forEach((ring) => {
                // @ts-ignore
                ring.vx *= (1 + accelerometer.x * .01);
                // @ts-ignore
                ring.vy *= (1 + accelerometer.y * .01);
            });
        }
    }

    changeTilt(gyroscope: Gyroscope) {
        // Придумать более адекватную формулу, потвикать, разобраться с ошибкой
        if (gyroscope.x !== undefined) {
            this.rings.forEach((ring) => {
                // @ts-ignore
                ring.vx += gyroscope.y * .02;
                // @ts-ignore
                ring.vy += gyroscope.x * .02;
            });
        }
    }

    blowAll(x: number, y: number) {
        this.rings.forEach((ring) => ring.blow(x, y));
    };

    defineBoundaries() {
        this.rings.forEach((ring) => {
            this.sticks.forEach((stick) => {
                const stickBegin = stick.x + stick.width / 2 - stick.hitboxWidth;
                const stickEnd = stick.x + stick.width / 2 + stick.hitboxWidth;

                const ringBegin = ring.x - ring.radius + ring.borderWidth;
                const ringEnd = ring.x + ring.radius - ring.borderWidth;

                // Интервал, в котором кольцо может ПОПАСТЬ на палку
                const stickTransitionInterval = stick.y + 20;

                const stickBottomOffset = 55 - ring.radius;

                // Условие, что кольцо на палке.
                const ringInStick =
                    (ring.vy >= 0 && ring.y <= stickTransitionInterval || ring.inStick == stick)
                    && ringBegin <= stickBegin
                    && ringEnd >= stickEnd
                    && ring.y >= stick.y
                    && ring.y <= stick.y + stick.heigth - stickBottomOffset;

                // Если кольцо на другой палке, то ничего не делать
                if (ring.inStick !== 0 && ring.inStick !== stick) {}

                // Если кольцо в пределах палки, делать внутри код только тогда,
                // когда палке не присвоен boundary
                else if (ringInStick) {
                    if (ring.inStick != stick) {
                        stick.ringCount += 1;
                        ring.boundary.x = [stickBegin, stickEnd];
                        ring.boundary.y = [0, stick.y + stick.heigth - 55 - stick.ringCount * ring.borderWidth];

                        ring.inStick = stick;
                    }
                }
                // Иначе кольцо не на палке
                else {
                    ring.boundary.x = [0, this.canvas.width];
                    ring.boundary.y = [0, this.canvas.height];

                    if (ring.inStick) {
                        stick.ringCount -= 1;
                        ring.inStick = 0;
                    }
                }
            });
        });
    }
}
