import stick from '@/assets/stick.png';
export class Stick {
    x = 0;
    y = 0;
    hitboxWidth = 7;
    // Здесь используется этот счётчик для верного расположения колец по оси y,
    // однако иногда встречается баг, когда выдувается более нижнее кольцо,
    // а верхние считают, будто оно ещё есть
    // Возможно для решения бага нужно реализовать массив колец на палке и каждому кольцу постоянно
    // динамически перезадавать границы
    ringCount = 0;
    // Нужно узнать нормальные размеры палок
    width = 100;
    heigth = 210;
    img: HTMLImageElement;
    imageLoaded = false;

    constructor(x: number, y: number) {
        this.x = x;
        this.y = y;

        this.img = new Image();
        this.img.onload = () => {
            this.imageLoaded = true;
        };
        this.img.src = stick;
    }

    drawStick(ctx: CanvasRenderingContext2D) {
        ctx.beginPath();

        if (this.imageLoaded) ctx.drawImage(this.img, this.x, this.y, this.width, this.heigth);
    }
}
