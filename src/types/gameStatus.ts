import { Ship, ShipType } from './ship';

export type GenericResponseStatus = {
    success: boolean,
    error?: number,
    message?: string,
}

type CellShootStatus = 0 | 1;
type FieldCell = [ShipType, CellShootStatus];

export type GameUpdateStatus  = {
    fieldMy: Array<FieldCell[]>,
    fieldEnemy: Array<FieldCell[]>,
    game: {
        id: string,
        invite: string,
        meReady: boolean,
        myTurn: boolean,
        status: number,
    },
    success: boolean,
    usedPlaces: Array<ShipType | null>,
}

export type GameStartStatus = {
    code: string,
    id: string,
    invite: string,
    success: true,
}

export type GameShipStatus = Ship & GenericResponseStatus;

