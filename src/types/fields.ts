import { CellStatus, ShipType } from './ship';

export type CellVisibility = 0 | 1;

export type FieldStore = {
    fieldMy: [[CellStatus, CellVisibility]][],
    fieldEnemy: [[CellStatus, CellVisibility]][],
    usedPlaces: ShipType[],
}
