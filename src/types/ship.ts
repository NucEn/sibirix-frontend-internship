export type ShipType =
    | '1-1'
    | '1-2'
    | '1-3'
    | '1-4'
    | '2-1'
    | '2-2'
    | '2-3'
    | '3-1'
    | '3-2'
    | '4-1';

export type Ship = {
    x?: 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9;
    y?: 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9;
    id: ShipType;
    orientation?: 'vertical' | 'horizontal';
    damage: number;
};

export type CellStatus = ShipType | 'empty' | 'hidden';
